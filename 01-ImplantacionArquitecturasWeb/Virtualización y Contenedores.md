﻿


> Written with [StackEdit](https://stackedit.io/).

## > VIRTUALIZACION Y CONTENEDORES

> La virtualización nos va a permitir tener varios sistemas operativos ejecutándose a la vez en una misma máquina física. Esto es posible gracias a las CPU’s modernas que soportan extensiones  **VT**  (Intel) o  **AMD-V**.

Hay distintos tipos de virtualización:

-   **ParaVirtualización**  :  **No**  hacen uso de las extensiones de virtualización de la CPU, con lo cual el rendimiento es notablemente inferior.  
    
-   **Virtualización completa**  : Hacen uso de las extensiones de virtualización de la CPU. Es el caso más común

Dentro de los tipos de virtualización, también podemos encontrarnos las que comparten un kernel común (LXC containers) y las que el  **host**  (Máquina física) y el  **guest**  tienen kernels diferentes.

Por lo general, las máquinas virtuales que comparten kernels tienen un mejor rendimiento y aprovechamiento de recursos.

La capa que gestiona la virtualización se conoce como  **Hypervisor**.

## Máquinas completas

Por máquinas completas nos referimos a aquéllas que no comparten kernel con el host.

-   Virtualbox  
    
-   VMware  
    
-   KVM

## Contenedores

-   XEN  
    
-   Docker
