# Servidores web: instalación e configuración básica

## Introducción


Ao remate desta actividade o alumnado deberá recoñecer:

* As distintas tecnoloxías empregadas en entornas de aplicacións web 
* O funcionamento dos servidores web
* Os parámetros básicos de configuración

Así mesmo será capaz de:

* Instalar servidores web 
* Configurar os servidores segundo os parámetros básicos 
* Documentar os procesos e configuracións realizadas


## Aplicacións Web vs Aplicacións de Escritorio


Unha aplicación de escritorio é unha aplicación que está instalada no equipo do usuario, executada directamente polo sistema operativo e cuxo rendemento depende directamente das prestacións do hardware.

Pola contra, unha aplicación web está aloxada nun servidor web e para a súa ‘execución’ se precisa dun navegador web e conexión á rede (sexa LAN ou internet).

## Introdución á web e ás arquitecturas

En informática a World Wide Web (WWW) é un sistema de distribución de documentos de hipertexto vía internet. Cun navegador web o usuario visualiza sitios web compostos de páxinas que poden conter textos, imaxes, vídeos e outros contidos multimedia, e navega a través destas páxinas mediante enlaces
Foi desenvolvida entre marzo de 1989 e decembro de 1990 polo inglés Tim Berners-Lee coa axuda do belga Robert Caillau mentres traballaban no CERN, en Xenebra.

Cando falamos de arquitecturas web estamos a falar dunha estrutura cliente-servidor, onde o cliente solicita algún recurso que o servidor ten ou pode obter.

O cliente sería o navegador e o servidor atenderá as peticións e devolverá eses recursos ao cliente por medio de respostas HTTP (protocolo de comunicación web).

Un servidor web ou servidor HTTP é un programa que ‘procesa’ aplicacións en linguaxes de servidor (facendo conexións e peticións a distintos recursos como intérpretes de linguaxes como PHP ou Servlets feitos en linguaxe Java) e devolve resultados en formato HTML aos clientes.


### Evolución das arquitecturas web

A historia das arquitecturas web é xa moi ampla, pasando por moitos enfoques sobre como construír unha aplicación web e continuarán a chegar novas ideas que farán evolucionar ao sector.

Nunha primeira etapa as páxinas (aplicacións web) tiñan basicamente todo tipo de código no mesmo ou mesmos documentos.

![Ilustración Architectura Web](img/archWebServer01.png)

En outra fase da evolución pasouse a traballar con obxectos, tendo o código xa mais modularidade. Isto facía posible a reutilización de código e un fácil mantemento do mesmo.

![Ilustración Architectura Web](img/archWebServer02.png)

Outro avance mais no senso de separar linguaxe e función foi o MVC (modelo vista controlador) no que se fai unha separación clara entre o que usuario ve no seu navegador (vistas), o código que procesa e ten a lóxica da aplicación (controlador) e os obxectos que forman a mesma (modelo). Dentro deste modelo evolucionouse ao chamado modelo MVC FrontController/Enroutador. A diferencia é que se ten un so controlador e este xera distintas accións.

![Ilustración Architectura Web](img/archWebServer03.png)

Hasta este momento todos os avances xurdiron dentro do servidor. Coa tecnoloxía AJAX se melloraron os tempos de espera entre a comunicación cliente-servidor.

Consiste basicamente en que o cliente segue a traballar cos datos que ten en pantalla o cliente e solicita en segundo plano información ao servidor sen que o usuario final perciba unha nova recarga da páxina cando os obtén. Poderíamos resumir AJAX como un conxunto de métodos e técnicas que permiten o intercambio de datos con un servidor web e actualizar partes da páxina web sen necesidade de recargar a páxina completa.

Imaxina unha interface dun xestor empresarial, cheo de menús, botóns e listaxes. Poderíamos actualizar unha listaxe sen recargar todo o resto da páxina.

Logo chegaron os frameworks do lado do cliente como Angular, pero de isto xa lles toca falar a outros en outros módulos.
Ímonos quedar entón con que un cliente (navegador) fará peticións a un servidor (programa que está a escoita nun equipo remoto, ou no mesmo).
Unha gráfica explicativa a modo xeral podería ser a seguinte:

![Ilustración Architectura Web](img/archWebServer04.png)

### Requisitos xerais para entornas cliente/servidor

O requisito que nos ocupa agora será ter un servidor web atendendo peticións dos clientes.

Logo, este servidor precisará de técnicas de comunicación con outros ‘elementos’ da cadea para procesar código e devolver resultados que o propio servidor enviará aos clientes. Por exemplo chamar ao intérprete de PHP que executa consultas a unha base de datos e devolve o listado ao servidor, quen llo envía ao cliente. Este proceso podémolo ver na gráfica anterior.

## Servidores Web

Existen moitos servidores web pero o que está instalado na meirande parte dos equipos é Apache. Está pensado e configurado para ter un maior rendemento en sistemas Linux e toda a documentación xira en torno a eles. De todas formas teñen un instalador para sistemas Windows.

En sistemas Windows o paquete que ven integrado é o chamado IIS (Internet Information Services). Non se trata só dun servidor web, tamén inclúe FTP, SMTP, NNTP e HTTP/HTTPS.

* Na seguinte [ligazón](https://es.wikipedia.org/wiki/Internet_Information_Services) tes información completa do paquete
* Nesta [páxina web](https://www.iis.net) tes todo o referente ao paquete de forma oficial

Na seguinte gráfica podes ver como está a loita polo mercado dos servidores web

![Gráfica sobre los servidores webs más activos](img/2017-webServers-activeSites.png)[Fuente](https://news.netcraft.com/archives/2017/06/27/june-2017-web-server-survey.html)

### Paquetes de ferramentas web

Existen tamén numerosos paquetes que fan a instalación completa de servidores web, xestores de bases de datos, intérpretes de linguaxes, servidores de correo e FTP, por exemplo.

Estas ferramentas son WampServer, Xampp ou Lamp. Para sistemas windows (W) e para sistemas Linux (L).

Este tipo de ferramentas son rápidas e sinxelas aínda que o control de arquivos para a súa configuración pode resultar algo mais complicado.

## Instalación de Apache en sistemas Linux

Partimos dunha máquina virtual con Ubuntu 16.04.1. Temos que abrir un terminal e executar o seguinte comando na consola:

```
sudo apt-get -y install apache2
```

Logo de introducir a contrasinal de root comezará o proceso de instalación de paquetes. Supoñemos que o alumnado ten coñecementos de comandos básicos na consola.

### Estrutura de directorios

Hai dúas estruturas de directorios que compre ter en conta para utilizar o servidor instalado.

Na ruta `/var/www/html` estarán os documentos que pode devolver aos clientes (navegadores) que lle soliciten recursos. Esta é unha ruta por defecto aínda que se poden configurar rutas alternativas como veremos en próximas unidades. Existe agora un arquivo `index.html` que é o que o servidor mostrará no caso de comunicarse con el na dirección IP do propio equipo ou no enderezo `127.0.0.1`

![Ilustración Captura Página Default de Apache2](img/capturaDefaultPageApache2.png)

Nesta páxina informase aos usuarios das rutas para os arquivos de configuración do servidor. Nun apartado posterior veremos os parámetros mais salientables e faremos pequenas edicións nos mesmos. Hai unha unidade didáctica que afonda nestes aspectos.


## Configuración básica de Apache

O arquivo principal de configuración de Apache é httpd.conf como norma xeral.

Trátase dun arquivo comentado e que pode ofrecer axuda ante calquera dúbida nas directivas de configuración.

Se as instalacións se fan en sistemas Linux dende a liña de comandos, como fixemos nós, o arquivo pode ter nomes distintos, por exemplo no noso caso chámase apache2.conf e está en /etc/apache2. Este arquivo terá variacións respecto dunha instalación descargando o tar.gz aparecendo algunhas directivas comentadas e facendo referencias a outros arquivos de configuración.

**Veremos aquí as directivas básicas que se inclúen por defecto en tódalas instalacións.**

* `ServerRoot`: define o directorio onde se ve toda a información de configuración e rexistro que precisa o servidor para o seu funcionamento. Debe ser absoluta.

* `Listen`: establece os portos nos que o server escoita as peticións. Por defecto será o porto 80. Tamén se poden especificar enderezos IP nos que se aceptará a conexión co servidor.

* `LoadModule`:se usa para cargar módulos que permiten o bo funcionamento e engaden funcionalidades ao servidor. A orde de inclusión é importante así que é importante non mudar nada sen telo moi claro.

* `User`: establece o userid usado polo servidor para responder as peticións. Calquera arquivo ao que non poida acceder este usuario será tamén inaccesible ao visitante da web.

* `Group`: este comando é similar ao anterior, establecendo o grupo no que o servidor responde ás peticións.

* `ServerAdmin`: establece o enderezo de contacto que o servidor engadirá en calquera mensaxe de erro. Deste xeito os usuarios poden enviar mensaxes ante incidencias.

* `ServerName`:pode usarse para establecer o nome da máquina do servidor distinto do nome real da mesma, ten que ser un nome DNS válido. Se non se dispón dun servidor DNS hai que configuralo no arquivo hosts do equipo. Se esta directiva non aparece o servidor responderá a peticións no enderezo 127.0.0.1 (localhost)

* `DocumentRoot`: establece o directorio no que estarán os arquivos que o servidor pode devolver. Se escribimos na barra de enderezo dun navegador http://127.0.0.1/probas/index.html o servidor irá á procura dun arquivo chamado index.html localizado na ruta c:/apache24/htdocs/probas. Esta ruta dependerá sempre do sistema e das configuracións de cada instalación.

* `Directory`: dentro destas etiquetas se agrupan directivas de configuración que só se aplican a eses directorios especificados. Dentro das directivas que se poñen por defecto sempre está Require co valor all granted, o que significa que o acceso ao directorio especificado como root está permitido a todos sen excepción. Falaremos mais disto no seu momento.

* `DirectoryIndex`: establece a lista de recursos a buscar cando o usuario so especifica unha barra (/) ao final dun directorio ao que ten acceso o servidor. Por exemplo, se poñemos http://localhost/ o servidor buscará un arquivo chamado index.html no caso de que esta directiva indique ese arquivo.


[Tes toda a información referente a directivas](https://httpd.apache.org/docs/2.4/es/mod/directives.html). Pero traballaremos algo máis sobre isto nunha actividade posterior.


## Reconocimiento

La autora original de este texto fue la Profesora Rosa María Gamallo Torres.

![Imagen Licencia](img/licencia-01.02.png)