# Creación de una WebApp con Maven

## Qué es Maven

Apache Maven es una herramienta de gestión y comprensión de proyectos de software. Basado en el concepto de un modelo de objeto de proyecto (POM - Project Object Model), Maven puede administrar la construcción, los informes y la documentación de un proyecto a partir de una información central.

[Los objetivos de Maven](https://maven.apache.org/what-is-maven.html) son los siguientes:

* Facilitar el proceso de construcción
* Proporcionar un sistema de construcción uniforme.
* Proporcionar información de calidad sobre el proyecto.
* Proporcionar directrices para el desarrollo de mejores prácticas.
* Permitir una migración transparente a nuevas funciones

Una de las capacidades de Maven que se hacen muy evidentes para el desarrollador de proyectos softwate son la **gestión de dependencias** (librerías externas que podemos utilizar y que maven descarga por nosotros), **la construcción y empaquetado** de un proyecto, listo para desplegar y la posible **automaticación del propio despliegue**.

## Instalación de Maven

Es posible instalar Apache Maven con gestores de paquetes como `apt` o `brew` (entre otros) y ejecutando por ejemplo el siguiente comando:

```bash
apt install maven
```

Sin embargo, en ambientes de desarrollo en ocasiones queremos realizar una instalación de una versión específica y de forma más controlada. En el siguinte [recurso adicional](https://maven.apache.org/install.html), se detalla la instalación de Maven descargado un fichero comprimido. Siguiendo posteriormente los siguientes pasos:

1. Descomprime el fichero en la ubicación deseada: `unzip apache-maven-3.6.3-bin.zip`
2. Añade el directorio `bin` de la carpeta descomprimida a la variable de entorno `PATH`.
3. Prueba que puedes ejecutar maven desde la terminal y desde cualquier directorio: `mvn -v`

El resultado de `mvn -v` será algo así:

```bash
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /opt/apache-maven-3.6.3
Java version: 1.8.0_45, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.8.5", arch: "x86_64", family: "mac"
```

## Uso de Maven. Phases

Para utilizar maven en un proyecto, éste debe contener en su carpeta un fichero `xml` donde **se describirá la naturaleza del propio proyecto**, sus dependencias, cómo se construirá, como se identificará, etc. Esto se especifica en el fichero `pom.xml`.

**Todo lo que maven produzca lo escribirá en una carpeta relativa a la raiz del proyecto llamada `target`**. Ahí generará por ejemplo, el software empaquetado, los ficheros compilados, los reports de los test automatizados, etc.

**Maven también creará un repositorio local** donde almacenará las librerías que se utilizarán. Concretamente en `$HOME/.m2`. Se podrán localizar librerías externas (entre otros lugares) en [MVNrepository.com](https://mvnrepository.com). La carpeta `$HONE/.m2` también contendrá ficheros configuración de configuración de maven para el usuario. 

Estas son las **fases de ciclo de vida** predeterminadas más comunes ejecutadas en un proyecto Maven. Recuerda que una fase del ciclo de vida de un proyecto podríamos entenderla como un conjunto de acciones que se realizan en un momento del desarrollo del proyecto: 

* `validate`: validar que el proyecto es correcto y toda la información necesaria está disponible.

* `compile`: compila el código fuente del proyecto.

* `test`: prueba el código fuente compilado usando un marco de prueba de unidad adecuado. Estas pruebas no deberían requerir que el código se empaquete o implemente.

* `package`: toma el código compilado y lo empaqueta en su formato distribuible, como un `JAR` o `WAR`.

* `integration-test`: procesa e implementa el paquete si es necesario en un entorno donde se puedan ejecutar pruebas de integración.

* `verify`: ejecutar cualquier verificación para verificar que el paquete sea válido y cumpla con los criterios de calidad.

* `install`: instala el paquete en el repositorio local, para usarlo como dependencia en otros proyectos localmente.

* `deploy`: hecho en un entorno de integración o lanzamiento, copia el paquete final en el repositorio remoto para compartirlo con otros desarrolladores y proyectos.

Hay otros dos ciclos de vida de Maven notables más allá de la lista predeterminada anterior. Son

* `clean`: limpia los artefactos creados por compilaciones anteriores.

* `site`: genera documentación del sitio para este proyecto.

En este [recurso adicional](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html#) encontrarás una guía rápida de maven: *Maven in 5 Minutes*

En este [recurso adicional]() encontrarás la guía de inicio de Maven (*Getting Started Guide*) donde se detalla las fases descritas anteriormente así con los conceptos fundamentales. Se estima que pueda llevar entre 30min o 40min, completarla.

## Creación con Maven de una WebApp

Maven nos permitirá crear un proyecto ya con la estructura de ficheros y directorios adecuada (arquetipo) para el tipo de proyecto que se quiera crear con él. Como una especie de *plantilla de proyecto* de la que partir.

Para crear una WebApp con maven se podrá utilizar el siguiente comando:

```bash
mvn archetype:generate \
	-DarchetypeArtifactId="maven-archetype-webapp" \
	-DarchetypeGroupId="org.apache.maven.archetypes" \
	-DarchetypeVersion="1.4"
```

Este comando realiza un proceso de creación del proyecto de forma interactiva:

1. Primero pedirá el `groupid` del proyecto. Será el identificador de quien desarrolla el proyecto software. Suele ser el dominio y sub-dominios de la empresa/organización descritos al revés. Por ejemplo: `net.iessanclemente.daw.developers2`
2. Después el `artifactId`. Será el identificador del componente software que pretendemos desarrollar en el proyecto.
3. Después la `version`. (se asume por ejemplo la versión por defecto).
4. Después `package`. Paquete java (se asume por ejemplo la versión por defecto).
5. Se confirma y se tiene la estructura en la carpeta actual del proyecto a iniciar.

Por ejempo algo así:

```bash
componente1
├── pom.xml
└── src
    └── main
        └── webapp
            ├── WEB-INF
            │   └── web.xml
            └── index.jsp
```

Se ha creado un proyecto ya con un fichero `pom.xml` pre establecido donde poder empezar a desarrollar.

Los diferentes IDEs provee de asistentes para realizar estas mismas acciones. Por otra parte, sea cual sea el IDE (Netbeans, Eclipse, VS Code, etc.) tienen la capacidad, bien de forma nativa o a través de extensiones, de comprender e interpretar el fichero `pom.xml` y configurar el entorno para comenzar a desarrollar con el IDE empleando todas sus utilirías.

## Levantar una WebApp con Maven empleando Jetty

### Qué es Jetty

[Jetty](https://www.eclipse.org/jetty/) es un servidor HTTP 100% basado en Java y un contenedor de Servlets escrito en Java.

El desarrollo de Jetty se enfoca en crear un servidor web sencillo, eficiente, empotrable y pluggable. El tamaño tan pequeño de Jetty lo hace apropiado para ofrecer servicios Web en una aplicación Java empotrada.

### Cómo utilizarlo con maven

Es posible levantar una aplicación Web usando el `jetty-maven-plugin`. Es necesario establecer en el `pom.xml` la configuración de este plugin.

```xml
<plugin>
  <groupId>org.eclipse.jetty</groupId>
  <artifactId>jetty-maven-plugin</artifactId>
  <version>9.1.3.v20140225</version>
    <configuration>
      <scanIntervalSeconds>10</scanIntervalSeconds>
      <jettyConfig>${jetty.configs}</jettyConfig>
      <reload>manual</reload>
      <contextPath>/</contextPath>
      <webAppConfig>
        <parentLoaderPriority>true</parentLoaderPriority>
      </webAppConfig>
    </configuration>
</plugin>
```

### Cómo levantar la WebApp

Utilizando maven y accediendo a `http://localhost:8080`

```bash
mvn clean install jetty:run 
```

En nueva terminal:

```bash
curl http://localhost:8080
```