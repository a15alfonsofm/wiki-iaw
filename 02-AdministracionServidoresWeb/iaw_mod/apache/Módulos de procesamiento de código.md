﻿


> Written with [StackEdit](https://stackedit.io/).
> # Uso de módulos en Apache 2.4
> Uno de los aspectos característicos del servidor HTTP Apache es su modularidad, Apache tiene un sinfín de características adicionales que si estuvieran siempre incluidas, harían de él un programa demasiado grande y pesado. En lugar de esto, Apache se compila de forma modular y se cargan en memoria sólo los módulos necesarios en cada caso.

Los módulos se guardan en la configuración de apache2 en dos directorios:

-   `/etc/apache2/mods-available/`: Directorio que contiene los módulos disponibles en la instalación actual.
-   `/etc/apache2/mods-enabled/`: Directorio que incluye mediante enlaces simbólicos al directorio anterior, los módulos que se van a cargar en memoria la próxima vez que se inicie Apache.

## Módulos de apache
Los módulos de apache se pueden encontrar de dos maneras, compilados dentro del ejecutable apache2 o compilados de forma individual como una biblioteca de enlace dinámico (con extensión .so). Para saber qué módulos incluye el ejecutable de nuestra instalación de apache, podemos utilizar la siguiente instrucción:

```
# apache2 -l	
Compiled in modules:
  core.c
  mod_so.c
  mod_watchdog.c
  http_core.c
  mod_log_config.c
  mod_logio.c
  mod_version.c
  mod_unixd.c

```

El resto de módulos disponibles para cargar en tiempo de ejecución se encuentran en el directorio  `/usr/lib/apache2/modules/`:

```
# ls /usr/lib/apache2/modules/

httpd.exp		mod_dav.so	    mod_proxy_fcgi.so
mod_access_compat.so	mod_dbd.so	    mod_proxy_fdpass.so
mod_actions.so		mod_deflate.so	    mod_proxy_ftp.so
...

```

Pueden parecer muchos, pero son sólo los módulos de la instalación estándar y se incluyen dentro del paquete  `apache2-data`. Hay otros muchos módulos que se distribuyen en paquetes separados, que en  _debian_  reciben el nombre  `libapache2-mod-*`:

```
# apt-cache search libapache2-mod
libapache2-mod-auth-ntlm-winbind - apache2 module for NTLM authentication against Winbind
libapache2-mod-upload-progress - upload progress support for the Apache web server
libapache2-mod-xforward - Apache module implements redirection based on X-Forward response header
...

```

## Utilización de módulos

Si vamos al directorio donde se ubican los módulos disponibles de Apache  `/etc/apache2/mods-available`  y hacemos un listado encontramos ficheros  `*.load`  y  `*.conf`.

Los ficheros con extensión  `load`  suelen incluir una línea con la directiva  `LoadModule`, por ejemplo:

```
# cat userdir.load 
LoadModule userdir_module /usr/lib/apache2/modules/mod_userdir.so

```

Además de cargar el módulo, en muchos casos es necesario realizar alguna configuración mediante directivas, por lo que en esos casos se existe un fichero con extensión  `.conf`.

Si queremos que Apache utilice cualquier módulo, lo que tendríamos que hacer es un enlace simbólico del fichero de extensión  `.load`  (y del  `.conf`  si existe) en el directorio  `/etc/apache2/mods-enabled`. Este enlace lo podemos hacer con la instrucción  `a2enmod`, por ejemplo:

```
# a2enmod userdir
Enabling module userdir.
To activate the new configuration, you need to run:
  systemctl restart apache2

```

Para desactivarlo (borrar el enlace simbólico) utilizamos la instrucción  `a2dismod`. después de utilizar estos comandos hay que reiniciar el servicio.

## Módulos activos por defect

Para ver los módulos activados en apache2:

```
# apache2ctl -M

Loaded Modules:
 core_module (static)
 so_module (static)
 watchdog_module (static)
 http_module (static)
 log_config_module (static)
 ...
```

# Módulos de Multiprocesamiento (MPM)

Los servidores web pueden ser configurado para manejar las peticiones de diferente forma, desde el punto de vista en que son creados y manejados los subprocesos necesarios que atienden a cada cliente conectado a este. En esta unidad vamos a explicar los MPM (Módulos de multiprocesamiento) que nos permiten configurar el servidor Web para gestionar las peticiones que llegan al servidor.

## event

Por defecto apache2 se configura con el MPM event, podemos ver el MPM que estamos utilizando con la siguiente instrucción:

```
# apachectl -V
...
Server MPM:     event
...

```

El MPM [event](https://httpd.apache.org/docs/2.4/mod/event.html) en versiones anteriores de apache2 era un módulo experimental, pero en Apache 2.4 se considera estable y mejora el funcionamiento del PMP worker. Este módulo usa procesos y al mismo tiempo hace uso de threads (también llamados hilos), es decir, combina las técnicas de forking y threading. Al iniciar Apache Event se crean varios procesos hijo y a su vez cada proceso hijo emplea varios threads. Con esto se consigue que cada proceso hijo pueda manejar varias peticiones simultaneas gracias a los threads.

En `/etc/apache2/mods-availables/mpm_event.conf` podemos configurar las opciones de configuración de este módulo:

-   `StartServers`: Número de procesos hijos que se crean al iniciar el servidor, por defecto 2.
-   `MinSpareThreads`: Mínimo número de hilos esperando para responder, por defecto 25.
-   `MaxSpareThreads`: Máximo número de hilos esperando para responder, por defecto 75.
-   `ThreadLimit`: Límite superior del número de hilos por proceso hijo que pueden especificarse, por defecto 64.
-   `ThreadsPerChild`: Número de hilos de cada proceso, por defecto 25.
-   `MaxRequestWorkers`: Límite de peticiones simultaneas que se pueden responder, por defecto 150.
-   `MaxConnectionsPerChild`: Límite en el número de peticiones que un proceso hijo puede atender durante su vida,por defecto 0 (no se indica).

## prefork

Este módulo crea diferentes procesos independientes para manejar las diferentes peticiones. Esta técnica de crear varios procesos se la denomina forking, de ahí el nombre mpm-prefork. Al iniciar Apache Prefork se crean varios procesos hijo y cada uno puede responder una petición.

Para cambiar de MPM tenemos que desactivar el actual y activar el nuevo módulo::

```
# a2dismod mpm_event
# a2enmod mpm_prefork
# service apache2 restart	

# apachectl -V
...
Server MPM:     prefork
...

```

En `/etc/apache2/mods-availables/mpm_prefork.conf` podemos configurar las opciones de configuración de este módulo:

-   `StartServers`: Número de procesos hijos que se crean al iniciar el servidor, por defecto 5.
-   `MinSpareServers`: Mínimo número de procesos esperando para responder, por defecto 5.
-   `MaxSpareServers`: Máximo número de procesos esperando para responder, por defecto 10.
-   `MaxRequestWorkers`: Límite de peticiones simultaneas que se pueden responder, por defecto 150.
-   `MaxConnectionsPerChild`: Límite en el número de peticiones que un proceso hijo puede atender durante su vida,por defecto 0 (no se indica).
