﻿# Configuración da seguridade no servidor web

# Introducción

Na actividade que nos ocupa aprenderanse os seguintes conceptos e manexo de destrezas sobre un servidor web:

* Configuración de **autenticación** e **control de acceso**.
* **Certificados dixitais**.
* Protocolo **HTTPS**.
* **Configuración** do servidor para asegurar as súas comunicacións co cliente.


# Control de acceso

O control de acceso en Apache permite evitar ou permitir o acceso de determinados computadores empregando o seu enderezo simbólico ou de transporte (IP).

Nas versións previas á 2.4, dito control de acceso facíase empregando as directivas `Order`, `Allow` e `Deny`. Pero estas directivas, a pesares de que aínda son recoñecidas na versión `2.4`, xa non deben ser empregadas, posto que desaparecerán en futuras versións.

Exemplo:

```
# Apache 2.2
Order Allow,Deny
Allow from all
Deny from example.com

# Apache 2.4+
<RequireAll>
    Require all granted
    Require not host example.com
</RequireAll>
```

### Directiva Require

**Permite indicar se unha petición ten acceso ao recurso solicitado (o document root, un directorio do document root...) ** en función da súa IP, enderezo simbólico, usuario autenticado, pertenza a un grupo de usuarios, etc.

* `Require all granted`: O acceso permítese sempre.

* `Require all denied`: O acceso non se permite nunca.

* `Require ip ip_ou_rango [ip_ou_rango] ...`: O acceso permítese desde esa IP.

* `Require host enderezo_simbólico`: O acceso permítese desde ese enderezo simbólico.

* `Require host local: O acceso permítese desde localhost.

* `Require user usuario1 [usuario2] ...`: O acceso se permite aos usuarios especificados.

* `Require group grupo1 [grupo2] ...`: O acceso se permite aos grupos especificados.

* `Require valid-user`: O acceso se permite aos grupos especificados.

* `Require not ip 192.168.205`: Cada una das opcións da directiva Require pode ser negada empregando a opción `not`.

* Se varias directivas `Require` se empregan nunha mesma sección sen estar contidas nun *contedor de autorización*, o acceso se permite se se cumpre algunha delas (equivalente a `RequireAny`).

#### Contenedores de autorización

Estes contedores poden aniñarse entre eles para indicar condicións máis complexas.

* `RequireAll`: Para que se permita o acceso ao recurso, deben cumprise todas as condicións indicadas por cláusulas Require que contén.

* `RequireAny`: Para que se permita o acceso ao recurso, debe cumprise algunha das condicións indicadas por cláusulas Require que contén.

* `RequireNone`: Para que se permita o acceso ao recurso, non debe cumprise ningunha das condicións indicadas por cláusulas Require que contén.

#### Ejemplos

1. Permitir o acceso desde todas as IP menos a `10.252.46.165`

```
<RequireAll>
    Require all granted
    Require not ip 10.252.46.165
</RequireAll>
```

2. Permitir o acceso a todos menos aos hosts de `xunta.gal`

```
<RequireAll>
   	Require all granted
   	Require not xunta.gal
</RequireAll>
```

3. Permitir o acceso desde todas as IP menos a 10.252.46.165 e a todos os hosts menos aos de `xunta.gal`:

```
<RequireAll>
    Require all granted
    Require not ip 192.168.205
        Require not host xunta.gal
</RequireAll>
```

4. Permitir o acceso ao directorio `/www/interno` ben aos hosts pertencentes a `xunta.gal` ou os que están na rede `192.168.0.0/24`.

```
<Directory "/www/interno">
   <RequireAny>
      Require host xunta.gal
      Require ip 192.168.0
   </RequireAny>
</Directory>
```

# Autorización y autenticación

A autenticación de usuarios faise cada vez máis a nivel de aplicacións web desarrolladas en linguaxes como PHP ou Python nas que se programa toda a lóxica de control de autenticación e acceso aos datos dos usuarios autorizados.

De tódolos xeitos, hai certos casos, como o control de acceso a unidades ou carpetas de rede que sirven ficheiros ou á páxina de administración dun router que é usual facelos a nivel do web server.

Para configurar o servidor Apache para que sexa capaz de autenticar aos usuarios e verificar a autorización do mesmo ao recurso solicitado, é necesario realizar as seguintes accións:

* Crear un ficheiro con usuarios (poderiamos ter tamén os usuarios nun LDAP ou nunha base de datos)
* Crear un ficheiro con grupos (se é necesario).
* Definir as directivas no ficheiro de configuración.

É necesario que tanto os ficheiros de usuarios como os de grupos, se atopen almacenados fóra dos directorios publicados, para que desta forma ninguén poida descargalos.

O control de acceso pode facerse para todos os ficheiros do sitio web ou para algúns ficheiros (mediante a directiva `<Files>`) ou directorios (mediante a directiva `<Directory>`).

### Autenticación Basic e Digest

A diferenza entre ambas é que no modo de autenticación Basic, os contrasinais envíanse desde o cliente encriptados cun hash pero en claro (nun texto plano), mentres que en Digest os contrasinais envíanse cifrados.
Ademáis o cifrado depende das variables de sistema date and time, así que cada vez que un usuario autentifícase no servidor a cadea cifrada enviada é diferente.

Se podería utilizar autenticación básica con https para que toda as comunicacións estean cifradas, pero nese caso recoméndase usar autenticación digest.

O módulo de Apache que controla a autenticación Basic é `auth_basic` e está habilitado por defecto, pola contra, o que controla a autenticación Digest é `auth_digest` e está deshabilitado por defecto.

#### Creación de usuarios

Para crear o ficheiro utilizamos o comando `htpasswd` (se a autenticación é Basic) ou `htdigest` (se a autenticación é Digest). A primeira vez que o utilicemos temos que poñerlle a opción `-c`, para que cre o arquivo. Creariamos o usuario da seguinte maneira:

```
htpasswd [-c] /etc/apache2/contr_basic xiana
```

Para `htdigest` temos que indicar, ademais, o dominio ao que pertence o usuario:

```
htdigest [-c] /etc/apache2/contr_digest profes xiana
```

Para empregar a autenticación por usuarios, debe estar habilitado o módulo authz_user, que o está por defecto.

#### Creación de grupos

Créase un ficheiro cun grupo por liña coa seguinte sintaxe:

```
nomeGrupo: usuario1 usuario2 usuario3 ...
```

Para empregar a autenticación por usuarios, debe estar habilitado o módulo `authz_groupfile`, que o está por defecto.

#### Directivas de restrición de acceso -> se aplican en los contextos: directory, .htaccess

* Directiva `AuthType`: Indicaremos o tipo de autenticación `Basic` ou `Digest`

* Directiva `AuthName`: Indica a área de influencia da autenticación. A cadea indicada nesta directiva é a que aparecerá no cadro de diálogo que se mostra ao usuario. En autenticación Digest, indica o dominio ao que ha de pertencer o usuario.

* Directiva `AuthUserFile`: Indica a ruta ao ficheiro no que se almacenan os usuarios e contrasinais.

* Directiva `AuthGroupFile`: Indica a ruta ao ficheiro no que se almacenan os grupos.

* Directiva `Require`: Indica que usuarios ou grupos teñen acceso á área. Se se indica valid-user significa que se permitirá o acceso a calquera dos usuarios que aparezan no ficheiro.

```
Require user profesor1 profesor2
Require group profes
```

### Ejemplos

1. Permitir o acceso a calquera usuario válido con autenticación `Basic`:

```
AuthType basic
AuthName "Zona privada IES Teis"
AuthUserFile "/etc/apache2/contr_basic"
Require valid-user
```

2. Permitir o acceso ao usuario `xiana` con autenticación `Digest`:   hai que activar o módulo `auth_digest` !!

```
AuthType Digest
AuthName "profes"
AuthBasicProvider file
AuthUserFile "/etc/apache2/contr_digest"
Require user xiana
```

3. Permitir o acceso ao grupo `profesores` con autenticación `Basic`:

```
AuthType Basic
AuthName "Para invitados"
AuthBasicProvider file
AuthUserFile "/etc/apache2/contr_basic"
AuthGroupFile "/etc/apache2/grupos"
Require group profesores
```

5. Permitir o acceso ao directorio `/www/docsadmin` ben ao usuario superadmin ou ben aos usuarios do grupo admins que accedan desde a rede `192.168.0.0/24:

```
<Directory "/www/docsadmin">
   <RequireAny>
      Require user superadmin
      <RequireAll>
         Require group admins
         Require ip 192.168.0
      </RequireAll>
   </RequireAny>
</Directory>
```


# Criptografía

A criptografía é a ciencia que estuda os algoritmos de cifrado e descifrado da información.

A información orixinal que debe protexerse denomínase texto plano (texto en claro). O **cifrado** é o proceso de converter o texto plano nun texto imposible de ler chamado texto cifrado ou *criptograma*. Para obter un texto cifrado, aplícase un **algoritmo de cifrado,** utilizando unha clave, ao texto plano:

![Ilustracion Cifrado](img/conf-serv-web-criptografia1.png)

Para recuperar a mensaxe orixinal, collerase o criptograma (texto cifrado), a clave e aplicarase o algoritmo de descifrado:

![Ilustracion Descifrado](img/conf-serv-web-criptografia2.png)

Os **principais obxectivos** que busca a criptografía son:

* **Integridade**: garante a exactitude da información contra a alteración, perda ou destrución da información. 
* **Confidencialidade**: a información revélase unicamente aos usuarios autorizados. 

* **Autenticación**: comprobación da identidade. 

* **Non repudio** (irrenunciabilidade ou vinculación): vincula un documento ou transacción a unha persoa ou equipo. 

**Dependendo do número de claves utilizadas** polos algoritmos de cifrado/descifrado, existen dous tipos de métodos criptográficos:

* Criptografía de **clave simétrica**, que emprega unha única clave.

* Criptografía de **clave asimétrica**, que emprega dúas claves.

Existen sistema chamados de criptografía híbrida, que empregan os dous sistemas (simétrico e asimétrico) na procura dun mellor rendemento.

### Sistemas criptográficos híbridos

Debido a que os sistemas de cifrado asimétricos son computacionalmente máis custosos que os simétricos e que estes últimos teñen o problema de intercambio de claves, inventáronse os sistemas de criptografía híbrida. Nestes sistemas híbridos, ao comezo da comunicación úsase un sistema de claves asimétricas para intercambiar de forma segura unha clave simétrica de sesión, que se usará para cifrar o resto da comunicación de forma máis eficiente.

Na seguinte figura pode verse un resumo do proceso:

1. *Primer paso*: Manuel envía a clave pública a Mónica.

2. *Segundo paso*: Mónica xera unha clave de sesión que é cifrada usando a clave pública de Manuel e a envía a Manuel. Este procede a descifrala usando a súa clave privada.

3. *Tercer paso*: Unha vez que os dous participantes da comunicación xa coñecen a clave de sesión, poden proceder a enviar mensaxes cifradas e descifralas usando a mesma clave (criptografía simétrica).

![Ilustración Criptografía Híbrida](img/conf-serv-web-criptografiaHibrida.png)

### Funcións Hash

Unha **función hash** é unha función matemática que converte un conxunto de datos noutro, normalmente nunha cadea de lonxitude fixa. O valor devolto pola función hash chámase **resumo**, **checksum**, suma hash ou **hash**. As funcións hash tamén son coñecidas como funcións resumo ou funcións digest. A seguinte imaxe exemplifica o funcionamento dunha función hash:

![Ilustración Funciones Hash](img/conf-serv-web-funcionamiento-FuncionHash.png) 

[Wikipedia](https://es.wikipedia.org/wiki/Archivo:Hash_function2-es.svg). Licenza CC BY-SA 3.0.

Unha función hash funciona nunha soa dirección; e polo tanto, é imposible calcular os datos orixinais a partir do valor resumo. Ao aplicar unha función hash a un documento ou ficheiro o valor resumo é unha cadea que identifica inequivocamente o contido. Este feito fai que as funcións hash se usen para:

* **Comprobación da integridade dun ficheiro**: as funcións hash permiten saber se un ficheiro sufriu modificacións; xa que, un cambio no contido do ficheiro tradúcese nun valor resumo diferente. 

* Identificación de ficheiros: como o hash depende do contido do arquivo é posible identificar arquivos con independencia do seu nome e/ou ubicación. 

* **Almacenamento de contrasinais**: en vez de gardar o contrasinal dos usuarios, gárdase o hash do contrasinal. Cando un usuario quere autenticarse indicando o seu contrasinal, calcúlase o hash e compárase co valor do hash almacenado.

### Certificados dixitais

Un **certificado dixital** é un documento electrónico, mediante o cal unha entidade (chamada de forma xenérica terceiro de **confianza**) garante o vínculo entra a identidade dun suxeito e unha clave pública (e polo tanto coa súa privada correspondente). Basicamente, é un documento que asocia unha identidade real cunha identidade dixital emitido por unha organización que se encargou de verificar esa asociación. 

Un certificado dixital contén xeralmente información do tipo:

* **Datos do usuario**: nome, apelidos, enderezo de correo electrónico, organización á que pertence, posto ou cargo profesional, etc.
* **Clave pública**, lonxitude e **algoritmo** usado na súa xeración
* **Datos da entidade** que emitiu a certificado.
* **Validez**
* **Uso/s** para os que foi rexistrada/autorizada a clave

A **firma dixital** permite asegurar a **integridade do certificado** e posibilitar as comprobacións de que non se realizaron cambios dende a súa emisión. **Para asinar o certificado** utilízase outra parella de claves xeradas con esta finalidade e asociadas á entidade certificadora.

**Para verificar a integridade dun certificado**, calcúlase o hash e compróbase co valor da firma (previo descifrado coa clave pública da entidade).

# Privacidade e autenticidade nas transmisións web

Desde o punto de vista de seguridade das comunicacións entre o servidor e o cliente, o protocolo HTTP apenas contempla os tres requirimentos necesarios para ser considerado seguro:
 
**Autenticación**: O cliente non ten forma de comprobar que o servidor ao que se conecta é realmente ao que quería conectarse. O servidor só pode comprobar a identidade do cliente mediante usuario e clave, método débil (sobre todo se as comunicacións van en claro) e mediante IP/nome de máquina de orixe. 

**Privacidade**: O protocolo HTTP viaxa en claro pola rede polo que o contido e os datos de usuario e clave poden ser facilmente capturados por terceiras persoas que se atopen nalgún punto entre o servidor e o cliente.

**Integridade**: Aínda que o protocolo HTTP usa o protocolo de transporte TCP, que garante que toda a información chega íntegra entre cliente e servidor, ningún dos dous protocolos evita que un terceiro poida interpoñerse entre cliente e servidor substituíndo as mensaxes dun e outro a vontade.
 
Aquí é onde entran en xogo **SSL (*Secure Sockets Layer*)** e **TLS (*Transport Layer Security*)**. SSL/TLS son protocolos criptográficos que permiten ter comunicacións seguras. Tanto SSL coma TLS son protocolos de nivel de aplicación que **a través do uso de certificados e criptografía híbrida** permiten establecer canles de comunicación SSL/TLS seguras polas que poden circular paquetes de datos de protocolos non seguros de nivel de aplicación, p.ex. HTTP (HTTPS), SMTP (SMTPS), etc.

O módulo `mod_ssl` pon a disposición do servidor web Apache, usando a biblioteca OpenSSL, os protocolos SSL (`v2` e `v3`) e TLS (considerado como `SSLv3.1`), que realizan o labor de **autenticación** (de clientes e servidores), **cifrado** das transmisións e **integridade** do seu contido:

* **Autenticación**: mediante **certificados dixitais**, que están asinados por unha autoridade certificadora (CA) recoñecida por ambos os extremos, que identifican inequivocamente ao seu posuidor. 

* **Privacidade**: mediante o uso de **algoritmos de cifrado**, toda comunicación entre cliente e servidor é só lexible para estes. 

* **Integridade**: mediante o uso de **algoritmos de resumo ou hash** impídese que esta sexa modificada de forma inadvertida

# HTTPs en Apache

O cifrado da comunicación entre o navegador e o servidor web Apache faise mediante o protocolo HTTPS, coas seguintes características:

-   Utiliza o protocolo SSL (actualmente TLS) para cifrado de datos. Para iso é preciso instalar o módulo `mod_ssl`
-   O servidor utiliza por defecto o porto 443/tcp.
-   Utiliza **mecanismos de cifrado de clave pública** e as claves públicas denomínanse **certificados**.
-   O formato dos certificados segue o estándar X.509 e normalmente son emitidos por unha entidade denominada  **Autoridad Certificadora**  (CA siglas en inglés).
-   Con HTTPS, a función principal da CA é demostrar a autenticidade do servidor e que pertence á persoa ou organización que o utiliza.  
    Segundo os criterios usados para comprobar a autenticidade do servidor se emiten diferentes tipos de certificados X.509 (actualmente se usa el llamado  _Extended Validation Certificate_).
-   O navegador contén unha lista de certificados de CA nas que confía e acepta inicialmente só os certificados dos servidores emitidos por algunha destas CA. Se non, o usuario pode aceptalos baixo a súa responsabilidade.
-   Unha vez aceptado o certificado dun servidor web, o navegador xenera unha clave simétrica, a cifra co certificado (clave pública) e a envía ao servidor que a descifra coa súa clave privada.  Así, o cliente e o servidor xa coñecen a clave simétrica que usarán para establecer unha conexión segura e para cifrar as comunicacións e os seus datos en cada conexión. 

O funcionamento esquemático de HTTPS é o seguinte:

![https](https://dc722jrlp2zu8.cloudfront.net/media/django-summernote/2018-03-28/1cfe2340-19b0-4f97-b177-81b5d3c0461d.png)


### Xeración de clave e certificado autofirmado

Para a xeración da clave privada e o certificado (clave pública) no servidor, empregamos as ferramentas ofrecidas pola libraría OpenSSL. Dita libraría está instalada en Ubuntu, para empregala en Windows, debemos empregar o executable `C:\Apache24\bin\openssl.exe.`

A continuación tes un resumo dos pasos a seguir. [Aquí](https://bitbucket.org/eduxunta/edu-wiki-dapw-iaw/src/master/02-AdministracionServidoresWeb/03-GeneracionDeCertificadosDigitales.md) tes unha descrición completa do proceso.

####**Xeración de clave privada :**

Indicando que se creará unha clave de 2048 bits:

```
openssl genrsa -out clave.key 2048
```

####**Solicitude de certificado**

Co seguinte comando se xera unha solicitude de certificado a partir da clave privada indicada, cos datos que se nos irán solicitando (os que rematan en `[]` son opcionais). Esta solicitude é a que se debe enviar á autoridade de certificación

```
openssl req –new –key clave.key –out solicitude.csr
```

####**Xeración de certificado autofirmado**

Para facer probas, queremos firmar o noso propio certificado, para o que empregamos o comando:

`openssl x509 –req –days 365 –in solicitude.csr –singkey clave.key –out certificado.crt`

Co que se xerará un certificado válido por 365 días, a partires da solicitude `solicitude.csr` e firmado coa clave `clave.key`.

### Configuración dun sitio virtual seguro

O proceso de creación dun sitio virtual seguro é o mesmo que para a creación dun sitio virtual, coa diferenza de que, no ficheiro de configuración do sitio virtual hai que incluír a maiores as liñas indicadas: `SSLEngine`,`SSLCertificateFile` e `SSLCertificateKeyFile`.

```
<IfModule mod_ssl.c>
  <VirtualHost *:443>
   ServerName sitio-seguro.com
   DocumentRoot /var/www/sitio-seguro

   SSLEngine on
   SSLCertificateFile /etc/ssl/certs/certificado.crt
   SSLCertificateKeyFile /etc/ssl/private/clave.key
  </VirtualHost>
</IfModule>
```

**Directivas:**

* `SSLEngine`. Cando se lle da valor on, indica que se habilita SSL para ese sitio.

* `SSLCertificateFile`. Indica a localización do certificado.

* `SSLCertificateKeyFile`. Indica a localización da clave privada.


### Reconocimento

Los autores originales de este exto fueron la Profesora Amalia Falcón Docampo, la Profesora Laura Fernández Nocelo y la Profesora Marta Rey López.

A información incluída nos apartados [Criptografía](#Criptografía), [Sistemas criptográficos híbridos](#Sistemas-criptográficos-híbridos) e [Privacidade e autenticidade nas transmisións web](#Privacidade-e-autenticidade-nas-transmisións-web) foi extraída de M. GONZÁLEZ REGAL. Actividade: Criptografía. Módulo: Seguridade e alta dispoñibilidade. Xunta de Galicia. 2015.

![Reconocimiento y licencia](img/licencia-02.01.png)ls
