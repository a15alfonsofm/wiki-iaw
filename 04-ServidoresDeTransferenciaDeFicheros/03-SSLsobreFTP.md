# Configuración de SSL sobre FTP

O protocolo FTP envía todos os datos, incluídos os usuarios e contrasinais, en claro. Para asegurar a conexión entre cliente e servidor, podemos cifrar dita comunicación empregando o protocolo SSL, como xa fixemos na actividade 2 da unidade 2 para o servidor web Apache. Na devandita actividade expúxose o procedemento a seguir para xerar un certificado dixital autofirmado, que agora empregaremos tamén para asegurar as comunicacións do servidor FTP.

Para configurar o servidor vsftpd para que funcione con SSL, debemos engadir no arquivo de configuración `/etc/vsftpd.conf` as seguintes liñas:

```
rsa_cert_file=/etc/ssl/certs/certificado-autofirmado.crt
rsa_private_key_file=/etc/ssl/private/clave-privada.key
ssl_enable=YES
```

Nas que lle indicamos que se habilita o protocolo SSL (`ssl_enable`), así coma a localización do certificado (`rsa_cert_file`) e da clave privada (`rsa_private_key_file`).

## Reconocimento

Los autores originales de este exto fueron la Profesora Amalia Falcón Docampo, la Profesora Laura Fernández Nocelo y la Profesora Marta Rey López.

![Reconocimiento y licencia](img/licencia-CSIFC03_MP0614_V000402_UD04_A02_Configuracion_FTP.png)